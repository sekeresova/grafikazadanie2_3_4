#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 5;
	myPenColor = Qt::black;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	QPainter painter(&image);
	if (event->button() == Qt::LeftButton) {
		painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		lastPoint = event->pos();
		painting = true;
		body.push_back(lastPoint);
		painter.drawPoint(lastPoint);
	}
	update();
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting) {
		drawLineTo(event->pos());
		lastPoint = event->pos();
		bodPoloha.setX(lastPoint.x());
		bodPoloha.setY(lastPoint.y());
	}
}

/* zapis vsetkych bodov pocas tahu */
void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	QPainter painter;
	if ((event->buttons() & Qt::LeftButton) && painting) {
		drawLineTo(event->pos());
		lastPoint = event->pos();
		bodyPosun.push_back(lastPoint);
	}
	for (int i = 0; i < body.size() - 1; i++) {
		DDA(body[i], body[i + 1], 255, 255, 255);
	}
	DDA(body[body.size() - 1], body[0], 255, 255, 255);
	/* ked chcem posunut kliknem, nasledne taham, tento bod sa zapise do vectora body (pre polygon), VYMAZAT */
	if (pocet == 0) body.pop_back();
	/* predosly tah vymazat */
	if (pocet > 1) {
		for (int i = 0; i < PosunuteBody.size() - 1; i++) {
			DDA(PosunuteBody[i], PosunuteBody[i + 1], 255, 255, 255);
		}
	}
	Posunutie();
	for (int i = 0; i < PosunuteBody.size() - 1; i++) {
		DDA(PosunuteBody[i], PosunuteBody[i + 1], 0, 0, 0);
	}
		pocet++;
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	QPainter painter;
	if (event->button() == Qt::LeftButton && painting) {
		lastPoint = event->pos();
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	modified = true;
	update();
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::vymazBody()
{
	body.clear();
	bodKruznice.clear();
	zrotovanyBod.clear();
	skalovanyBod.clear();
	bodyPosun.clear();
	skosenyBod.clear();
	PosunuteBody.clear();
	preklopeneBody.clear();
}

void PaintWidget::vymazHlavneBody()
{
	body.clear();
}

void PaintWidget::BodyKruznica(int pocet, int polomer)
{
	QPainter painter(&image);
	int xs = image.width();
	int ys = image.height();
	float X, Y;
	painter.setPen(QPen(Qt::black, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(Qt::black));

	for (int i = 0; i < pocet; i++) {
		X = (xs / 2) + polomer * cos(i * 2 * M_PI / pocet);
		Y = (ys / 2) + polomer * sin(i * 2 * M_PI / pocet);
		bodKruznice.push_back(QPoint(round(X), round(Y)));
		painter.drawEllipse(round(X), round(Y), 3, 3);
	}
	update();
}

bool sortFunkciaTH(const QVector<float>& a, const QVector<float>& b)
{
	if (a[1] == b[1]) return a[0] < b[0];
	else if (a[1] == b[1] && a[0] == b[0]) return a[3] < b[3];
	else return a[1] < b[1];
}

void PaintWidget::naplnTH(vector<QPoint> body)
{
	/* 2D pole = TH , riadok reprezentuje jednu hranu, stlpce su info ktore ma dana hrana */
	stlpec = 4;
	float dx, dy;
	int pocet = 0;
	riadok = body.size();
	body.push_back(body[0]);
		for (int i = 0; i < riadok; i++) {
			QVector<float> row;
			/* odsekavam koncovy bod */
			if (dy != 0 && (body[i + 1].y() > body[i].y())) {
				dy = body[i + 1].y() - body[i].y();
				dx = body[i + 1].x() - body[i].x();
				w = dx / dy;
				row.push_back(body[i].x());				//Xz
				row.push_back(body[i].y());				//Yz	
				row.push_back(body[i + 1].y() - 1);		//Yk
				row.push_back(w);						//w - obratena hodnota smernice
				TH.push_back(row);
				pocet++;
			}
			else if (dy != 0 && (body[i + 1].y() < body[i].y())) {
				dy = body[i].y() - body[i + 1].y();
				dx = body[i].x() - body[i + 1].x();
				w = dx / dy;
				row.push_back(body[i + 1].x());			//Xz
				row.push_back(body[i + 1].y());			//Yz	
				row.push_back(body[i].y() - 1);			//Yk
				row.push_back(w);						//w - obratena hodnota smernice
				TH.push_back(row);
				pocet++;
			}
		}
		qSort(TH.begin(), TH.end(), sortFunkciaTH);
}

void PaintWidget::ScanLine(int red, int green, int blue)
{
	QPainter painter(&image);
	color.setRgb(red, green, blue);
	painter.setPen(QPen(color, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(color));
	int pocet = 0;
	QVector<float> priesecnikX;
	QVector<QVector<float>> TAH;

	float Ya = TH[0][1];
	float Ymax = TH[riadok - 1][2];
	while (Ya <= Ymax) {
		/* do TAH ulozim hrany ktore, Ya = Yz */
		for (int i = 0; i < riadok; i++) {
			QVector<float> row;
			if (TH[i][1] == Ya) {
				row.push_back(TH[i][0]);		//Xz
				row.push_back(TH[i][1]);		//Yz
				row.push_back(TH[i][2]);		//Yk
				row.push_back(TH[i][3]);		//w - obratena hodnota smernice
				TAH.push_back(row);
				pocet++;
				TAH.resize(pocet);
			}
		}
		/* vyratanie priesecnikov */
		priesecnikX.clear();
		for (int i = 0; i < pocet; i++) {
			/* Xa = W*Ya - W*Yz + Xz */
			priesecnikX.push_back((TAH[i][3] * Ya) - (TAH[i][3] * TAH[i][1]) + TAH[i][0]);
		}
		qSort(priesecnikX);
		/* Pospajanie priesecnikov */
		for (int i = 0; i < pocet - 1; i = i + 2) {
			/* TAH[i][1] = Ya */
			painter.drawLine(round(priesecnikX[i]), round(Ya), round(priesecnikX[i + 1]), round(Ya));
		}
		/* Vymazanie hrany z TAH ak Yk == Ya */
		for (int i = 0; i < TAH.size(); i++) {
				if (TAH[i][2] == Ya) {
					for (int j = 3; j >= 0; j--) {
						//TAH[i].erase(TAH[i].begin() + j);
						TAH[i].remove(j);
						if (j == 0) TAH.remove(i);
					}
					pocet--;
					TAH.resize(pocet);
				}
		}
		Ya++;
	}
}

void PaintWidget::DDA(QPoint a, QPoint b, int red = 255, int green = 255, int blue = 255)
{
	QPainter painter(&image);
	color.setRgb(red, green, blue);
	painter.setPen(QPen(color, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(color));

	float  m, xx, yy, deltaY, deltaX, X, Y;
	int x1, x2, y1, y2;
	x1 = a.x();
	y1 = a.y();
	x2 = b.x();
	y2 = b.y();
	deltaX = x2 - x1;
	deltaY = y2 - y1;
	if (fabs(deltaY) <= fabs(deltaX)) {
		m = (deltaY / deltaX);
		if (x1 < x2) { xx = x1; yy = y1; X = x2; }
		else { xx = x2; yy = y2; X = x1; }
		painter.drawEllipse((int)xx, (int)yy, 3, 3);
		while (xx < X) {
			xx = xx + 1.0;
			yy = yy + m;
			painter.drawEllipse(xx, (int)yy, 3, 3);
		}
	}
	else {
		m = (deltaX / deltaY);
		if (y1 < y2) { xx = x1; yy = y1; Y = y2; }
		else { xx = x2; yy = y2; Y = y1; }
		painter.drawEllipse((int)xx, (int)yy, 3, 3);
		while (yy < Y) {
			xx = xx + m;
			yy = yy + 1.0;
			painter.drawEllipse((int)xx, yy, 3, 3);
		}
	}
	update();
}

void PaintWidget::Bresenham(QPoint a, QPoint b, int red = 255, int green = 255, int blue = 255)
{
	QPainter painter(&image);
	int deltaX, deltaY, px, py, X2, Y2, xx, yy, k1, k2, k11, k22;
	
	painter.setPen(QPen(QColor(red, green, blue), myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(QColor(red, green, blue)));
	
	int x1, x2, y1, y2;
	x1 = a.x();
	y1 = a.y();
	x2 = b.x();
	y2 = b.y();
	deltaX = x2 - x1;
	deltaY = y2 - y1;

	px = 2 * fabs(deltaY) - fabs(deltaX);
	py = 2 * fabs(deltaX) - fabs(deltaY);
	k1 = 2 * fabs(deltaY);
	k2 = 2 * fabs(deltaY) - 2 * fabs(deltaX);
	k11 = 2 * fabs(deltaX);
	k22 = 2 * fabs(deltaX) - 2 * fabs(deltaY);
	if (fabs(deltaY) <= fabs(deltaX)) {
		if (deltaX >= 0) {
			xx = x1;
			yy = y1;
			X2 = x2;
		}
		else {
			xx = x2;
			yy = y2;
			X2 = x1;
		}
		painter.drawEllipse(xx, yy, 5, 5);
		while (xx < X2) {
			xx = xx + 1;
			if (px > 0) {
				if ((deltaX < 0 && deltaY < 0) || (deltaX > 0 && deltaY > 0)) yy = yy + 1;
				else yy = yy - 1;
				px = px + k2;
			}
			else px = px + k1;
			painter.drawEllipse(xx, yy, 5, 5);
		}
	}
	else {
		if (deltaY >= 0) {
			xx = x1;
			yy = y1;
			Y2 = y2;
		}
		else {
			xx = x2;
			yy = y2;
			Y2 = y1;
		}
		painter.drawEllipse(xx, yy, 5, 5);
		while (yy < Y2) {
			 yy = yy + 1;
			if (py > 0) {
				if ((deltaX < 0 && deltaY < 0) || (deltaX > 0 && deltaY > 0)) xx = xx + 1;
				else xx = xx - 1;
					py = py + k22;
				}
			else py = py + k11;
			painter.drawEllipse(xx, yy, 5, 5);
			}
		}
}


void PaintWidget::Posunutie()
{
	PosunuteBody.clear();
	int X, Y;
	int rozdielX = (bodyPosun[bodyPosun.size() - 1].x() - bodyPosun[0].x());
	int rozdielY = (bodyPosun[bodyPosun.size() - 1].y() - bodyPosun[0].y());
	for (int i = 0; i < body.size(); i++) {
		X = body[i].x() + rozdielX;
		Y = body[i].y() + rozdielY;
		PosunuteBody.push_back(QPoint(X, Y));
	}
	PosunuteBody.push_back(PosunuteBody[0]);
}

void PaintWidget::RotaciaVlavo(QString znamienko, double hodnota)
{
	int Sx = body[0].x();
	int Sy = body[0].y();
	int X, Y;
	zrotovanyBod.push_back(body[0]);
	for (int i = 1; i < body.size(); i++) {
		if (znamienko == ("*")) {
			X = round(((body[i].x() - Sx) * cos(-M_PI * hodnota) - (body[i].y() - Sy) * sin(-M_PI * hodnota) + Sx));
			Y = round(((body[i].x() - Sx) * sin(-M_PI * hodnota) + (body[i].y() - Sy) * cos(-M_PI * hodnota) + Sy));
			zrotovanyBod.push_back(QPoint(X, Y));
		}
		else {
			X = round(((body[i].x() - Sx) * cos(-M_PI / hodnota) - (body[i].y() - Sy) * sin(-M_PI / hodnota) + Sx));
			Y = round(((body[i].x() - Sx) * sin(-M_PI / hodnota) + (body[i].y() - Sy) * cos(-M_PI / hodnota) + Sy));
			zrotovanyBod.push_back(QPoint(X, Y));
		}
	}
	//zrotovanyBod.push_back(zrotovanyBod[0]);
}

void PaintWidget::RotaciaVpravo(QString znamienko, double hodnota)
{
	int Sx = body[0].x();
	int Sy = body[0].y();
	int X, Y;
	zrotovanyBod.push_back(body[0]);
	for (int i = 1; i < body.size(); i++) {
		if (znamienko == ("*")) {
			X = round(((body[i].x() - Sx) * cos(-M_PI * hodnota) + (body[i].y() - Sy) * sin(-M_PI * hodnota) + Sx));
			Y = round( ((body[i].x() - Sx) * sin(M_PI * hodnota) + (body[i].y() - Sy) * cos(M_PI * hodnota) + Sy));
			zrotovanyBod.push_back(QPoint(X, Y));
		}
		else {
			X = round(((body[i].x() - Sx) * cos(-M_PI / hodnota) + (body[i].y() - Sy) * sin(-M_PI / hodnota) + Sx));
			Y = round(((body[i].x() - Sx) * sin(M_PI  / hodnota) + (body[i].y() - Sy) * cos(M_PI / hodnota) + Sy));
			zrotovanyBod.push_back(QPoint(X, Y));
		}
	}
	//zrotovanyBod.push_back(zrotovanyBod[0]);
}

void PaintWidget::Skalovanie(double koeficient)
{
	int X, Y, rozdielX, rozdielY;
	skalovanyBod.push_back(body[0]);
	for (int i = 1; i < body.size(); i++) {
		rozdielX = body[i].x() - body[0].x();
		rozdielY = body[i].y() - body[0].y();
		X = round(koeficient * rozdielX + body[i].x());
		Y = round(koeficient * rozdielY + body[i].y());
		skalovanyBod.push_back(QPoint(X,Y));
	}
	//skalovanyBod.push_back(body[0]);
}

void PaintWidget::Preklopenie(int smer, int red, int green, int blue)
{
	preklopeneBody.clear();
	int a, b, c, smernicaA, smernicaB, X, Y;
	QPoint horizontStart, horizontEnd, normala;
	QPoint vertikalStart, vertikalEnd;
	float dlzka;

	if (smer == 0) {
		horizontStart.setX(0);
		horizontStart.setY(body[0].y());
		horizontEnd.setX(image.width());
		horizontEnd.setY(body[0].y());
		DDA(horizontStart, horizontEnd, 255, 0, 0);
		smernicaA = (body[0].x() + 5) - body[0].x();
		smernicaB = 0;

		a = -1 * smernicaB;
		b = smernicaA;
		c = (-a * body[0].x()) - (b * body[0].y());

		for (int i = 0; i < body.size(); i++) {
			dlzka = ((a * body[i].x() + b * body[i].y() + c) / (a * a + b * b));
			X = round(body[i].x() - (2 * a * dlzka));
			Y = round(body[i].y() - (2 * b * dlzka));
			preklopeneBody.push_back(QPoint(X, Y));
		}
		/* ak chcem scanLine tak nepridavam posledny bod, inak ano */
		//preklopeneBody.push_back(preklopeneBody[0]);
	}
	else if (smer == 1) {
		vertikalStart.setX(body[0].x());
		vertikalStart.setY(0);
		vertikalEnd.setX(body[0].x());
		vertikalEnd.setY(image.height());
		DDA(vertikalStart, vertikalEnd, 255, 0, 0);
		smernicaA = 0;
		smernicaB = (body[0].y() + 3) - body[0].y();

		a = -1 * smernicaB;
		b = smernicaA;
		c = (-a * body[0].x()) - (b * body[0].y());

		for (int i = 0; i < body.size(); i++) {
			dlzka = ((a * body[i].x() + b * body[i].y() + c) / (a * a + b * b));
			X = round(body[i].x() - (2 * a * dlzka));
			Y = round(body[i].y() - (2 * b * dlzka));
			preklopeneBody.push_back(QPoint(X, Y));
		}
		//preklopeneBody.push_back(preklopeneBody[0]);
	}
}

void PaintWidget::Skosenie(double d, double smer)
{
	int X, Y;
	if (smer == 0) {
		/* prvy bod je rovnaky, nemeni sa, ulozim */
		skosenyBod.push_back(body[0]);
		for (int i = 1; i < body.size(); i++) {
			X = round(body[i].x() + (d * abs(body[0].y() - body[i].y())));
			Y = body[i].y();
			skosenyBod.push_back(QPoint(X,Y));
		}
		/* posledny bod je rovnaky ako prvy, kvoli DDA */
		//skosenyBod.push_back(body[0]);
	}
	else if (smer == 1) {
		skosenyBod.push_back(body[0]);
		for (int i = 0; i < body.size(); i++) {
			X = body[i].x();
			Y = round(body[i].y() + (d * (body[0].x() - body[i].x())));
			skosenyBod.push_back(QPoint(X,Y));
		}
		//skosenyBod.push_back(body[0]);
	}
}