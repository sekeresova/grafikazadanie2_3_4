#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(700, 700);
}

MyPainter::~MyPainter()
{
}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
	paintWidget.vymazBody();
	paintWidget.newImage(700, 700);
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Kresli()
{
	QPoint a, b;
	vector<QPoint> body;
	body = paintWidget.DajBody();
	body.push_back(body[0]);
	for (int i = 0; i < body.size() - 1; i++) {
		a = body[i];
		b = body[i + 1];
		paintWidget.DDA(a, b, ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
}	

bool MyPainter::ActionVypln()
{
	vector<QPoint> body;
	body = paintWidget.DajBody();
	for (int i = 0; i < body.size() - 1; i++) {
		paintWidget.DDA(body[i], body[i + 1], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
	paintWidget.DDA(body[body.size() - 1], body[0], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	paintWidget.naplnTH(body);
	paintWidget.ScanLine(ui.spinBox_5->value(), ui.spinBox_6->value(), ui.spinBox->value());
}

void MyPainter::ActionKruznica()
{
	vector<QPoint> bodKruznice;
	paintWidget.BodyKruznica(ui.spinBox_pocet_bodov->value(), ui.spinBox_polomer->value());
	bodKruznice = paintWidget.DajBodKruznice();
	bodKruznice.push_back(bodKruznice[0]);
	
	if (ui.comboBox_algoritmus->currentIndex() == 0) {
		for (int i = 0; i < bodKruznice.size() - 1; i++) {
			paintWidget.DDA(bodKruznice[i], bodKruznice[i + 1], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
		}
	}
	else if (ui.comboBox_algoritmus->currentIndex() == 1)
	{
		for (int i = 0; i < bodKruznice.size() - 1; i++) {
			paintWidget.Bresenham(bodKruznice[i], bodKruznice[i + 1], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
		}
	}
}

void MyPainter::ActionRotaciaVlavo()
{
	paintWidget.clearImage();
	paintWidget.RotaciaVlavo(ui.lineEdit_znamienko->text(), ui.doubleSpinBox_hodnota->value());
	QPoint a, b;
	vector<QPoint> zrotovanyBod;
	zrotovanyBod = paintWidget.DajZrotovanyBod();
	for (int i = 0; i < zrotovanyBod.size() - 1; i++) {
		a = zrotovanyBod[i];
		b = zrotovanyBod[i + 1];
		paintWidget.DDA(a, b, ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
	paintWidget.DDA(zrotovanyBod[zrotovanyBod.size() - 1], zrotovanyBod[0], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	paintWidget.naplnTH(zrotovanyBod);
	paintWidget.ScanLine(ui.spinBox_5->value(), ui.spinBox_6->value(), ui.spinBox->value());
}

void MyPainter::ActionRotaciaVpravo()
{
	paintWidget.clearImage();
	paintWidget.RotaciaVpravo(ui.lineEdit_znamienko->text(), ui.doubleSpinBox_hodnota->value());
	QPoint a, b;
	vector<QPoint> zrotovanyBod;
	zrotovanyBod = paintWidget.DajZrotovanyBod();
	for (int i = 0; i < zrotovanyBod.size() - 1; i++) {
		a = zrotovanyBod[i];
		b = zrotovanyBod[i + 1];
		paintWidget.DDA(a, b, ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
	paintWidget.DDA(zrotovanyBod[zrotovanyBod.size() - 1], zrotovanyBod[0], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	paintWidget.naplnTH(zrotovanyBod);
	paintWidget.ScanLine(ui.spinBox_5->value(), ui.spinBox_6->value(), ui.spinBox->value());
}

void MyPainter::ActionSkalovanie()
{
	paintWidget.clearImage();
	vector<QPoint> skalovaneBody;
	paintWidget.Skalovanie(ui.doubleSpinBox_skalovanie->value());
	skalovaneBody = paintWidget.DajSkalovanyBod();
	for (int i = 0; i < skalovaneBody.size() - 1; i++) {
		paintWidget.DDA(skalovaneBody[i], skalovaneBody[i + 1], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
	paintWidget.DDA(skalovaneBody[skalovaneBody.size() - 1], skalovaneBody[0], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	paintWidget.naplnTH(skalovaneBody);
	paintWidget.ScanLine(ui.spinBox_5->value(), ui.spinBox_6->value(), ui.spinBox->value());
}

void MyPainter::ActionPreklopenie()
{
	paintWidget.clearImage();
	vector<QPoint> preklopeneBody;
	paintWidget.Preklopenie(ui.comboBox_preklopenie->currentIndex(), ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	preklopeneBody = paintWidget.DajPreklopeneBody();
	for (int i = 0; i < preklopeneBody.size() - 1; i++) {
		paintWidget.DDA(preklopeneBody[i], preklopeneBody[i + 1], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
	paintWidget.DDA(preklopeneBody[preklopeneBody.size() - 1], preklopeneBody[0], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	paintWidget.naplnTH(preklopeneBody);
	paintWidget.ScanLine(ui.spinBox_5->value(), ui.spinBox_6->value(), ui.spinBox->value());
}

void MyPainter::ActionSkosenie()
{
	paintWidget.clearImage();
	paintWidget.Skosenie(ui.doubleSpinBox_skosenie->value(), ui.comboBox_skosenie->currentIndex());
	vector<QPoint> skosenyBod;
	skosenyBod = paintWidget.DajSkosenyBod();
	for (int i = 0; i < skosenyBod.size() - 1; i++) {
		paintWidget.DDA(skosenyBod[i], skosenyBod[i + 1], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
	paintWidget.DDA(skosenyBod[skosenyBod.size() - 1], skosenyBod[0], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	paintWidget.naplnTH(skosenyBod);
	paintWidget.ScanLine(ui.spinBox_5->value(), ui.spinBox_6->value(), ui.spinBox->value());
}
