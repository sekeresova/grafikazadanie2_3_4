/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QWidget *centralWidget;
    QFormLayout *formLayout_2;
    QGridLayout *gridLayout_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QFormLayout *formLayout;
    QLabel *label_9;
    QComboBox *comboBox_algoritmus;
    QSpinBox *spinBox_polomer;
    QLabel *label_20;
    QSpinBox *spinBox_pocet_bodov;
    QLabel *label_5;
    QSpinBox *spinBox_2;
    QLabel *label_6;
    QSpinBox *spinBox_3;
    QLabel *label_7;
    QSpinBox *spinBox_4;
    QPushButton *pushButton;
    QLabel *label_4;
    QLabel *label_10;
    QSpinBox *spinBox_5;
    QLabel *label_11;
    QSpinBox *spinBox_6;
    QLabel *label_12;
    QSpinBox *spinBox;
    QPushButton *pushButton_7;
    QSpacerItem *verticalSpacer;
    QGridLayout *gridLayout;
    QLabel *label_15;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_14;
    QDoubleSpinBox *doubleSpinBox_hodnota;
    QLineEdit *lineEdit_znamienko;
    QLabel *label;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_16;
    QDoubleSpinBox *doubleSpinBox_skalovanie;
    QPushButton *pushButton_4;
    QSpacerItem *verticalSpacer_3;
    QLabel *label_18;
    QDoubleSpinBox *doubleSpinBox_skosenie;
    QComboBox *comboBox_skosenie;
    QPushButton *pushButton_5;
    QSpacerItem *verticalSpacer_4;
    QLabel *label_13;
    QComboBox *comboBox_preklopenie;
    QPushButton *pushButton_6;
    QLabel *label_19;
    QLabel *label_8;
    QPushButton *pushButton_kruznica;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEffects;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(657, 784);
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        formLayout_2 = new QFormLayout(centralWidget);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(5);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        gridLayout_2->setContentsMargins(10, 10, 10, 10);
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 344, 703));
        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_2->addWidget(scrollArea, 0, 0, 1, 1);


        formLayout_2->setLayout(0, QFormLayout::FieldRole, gridLayout_2);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_9);

        comboBox_algoritmus = new QComboBox(centralWidget);
        comboBox_algoritmus->setObjectName(QStringLiteral("comboBox_algoritmus"));

        formLayout->setWidget(2, QFormLayout::FieldRole, comboBox_algoritmus);

        spinBox_polomer = new QSpinBox(centralWidget);
        spinBox_polomer->setObjectName(QStringLiteral("spinBox_polomer"));
        spinBox_polomer->setMaximum(900);

        formLayout->setWidget(3, QFormLayout::FieldRole, spinBox_polomer);

        label_20 = new QLabel(centralWidget);
        label_20->setObjectName(QStringLiteral("label_20"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_20);

        spinBox_pocet_bodov = new QSpinBox(centralWidget);
        spinBox_pocet_bodov->setObjectName(QStringLiteral("spinBox_pocet_bodov"));
        spinBox_pocet_bodov->setMinimum(3);
        spinBox_pocet_bodov->setMaximum(30);

        formLayout->setWidget(4, QFormLayout::FieldRole, spinBox_pocet_bodov);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(7, QFormLayout::LabelRole, label_5);

        spinBox_2 = new QSpinBox(centralWidget);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setMaximum(255);

        formLayout->setWidget(7, QFormLayout::FieldRole, spinBox_2);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_6);

        spinBox_3 = new QSpinBox(centralWidget);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setMaximum(255);

        formLayout->setWidget(8, QFormLayout::FieldRole, spinBox_3);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(9, QFormLayout::LabelRole, label_7);

        spinBox_4 = new QSpinBox(centralWidget);
        spinBox_4->setObjectName(QStringLiteral("spinBox_4"));
        spinBox_4->setMaximum(255);

        formLayout->setWidget(9, QFormLayout::FieldRole, spinBox_4);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout->setWidget(10, QFormLayout::FieldRole, pushButton);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(11, QFormLayout::LabelRole, label_4);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout->setWidget(12, QFormLayout::LabelRole, label_10);

        spinBox_5 = new QSpinBox(centralWidget);
        spinBox_5->setObjectName(QStringLiteral("spinBox_5"));
        spinBox_5->setMaximum(255);

        formLayout->setWidget(12, QFormLayout::FieldRole, spinBox_5);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout->setWidget(13, QFormLayout::LabelRole, label_11);

        spinBox_6 = new QSpinBox(centralWidget);
        spinBox_6->setObjectName(QStringLiteral("spinBox_6"));
        spinBox_6->setMaximum(255);

        formLayout->setWidget(13, QFormLayout::FieldRole, spinBox_6);

        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));

        formLayout->setWidget(14, QFormLayout::LabelRole, label_12);

        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMaximum(255);

        formLayout->setWidget(14, QFormLayout::FieldRole, spinBox);

        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));

        formLayout->setWidget(15, QFormLayout::FieldRole, pushButton_7);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(16, QFormLayout::LabelRole, verticalSpacer);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_15 = new QLabel(centralWidget);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout->addWidget(label_15, 1, 2, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout->addWidget(label_14, 1, 1, 1, 1);

        doubleSpinBox_hodnota = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_hodnota->setObjectName(QStringLiteral("doubleSpinBox_hodnota"));

        gridLayout->addWidget(doubleSpinBox_hodnota, 2, 2, 1, 1);

        lineEdit_znamienko = new QLineEdit(centralWidget);
        lineEdit_znamienko->setObjectName(QStringLiteral("lineEdit_znamienko"));

        gridLayout->addWidget(lineEdit_znamienko, 2, 1, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);


        formLayout->setLayout(17, QFormLayout::SpanningRole, gridLayout);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        formLayout->setWidget(20, QFormLayout::LabelRole, pushButton_2);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        formLayout->setWidget(20, QFormLayout::FieldRole, pushButton_3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(21, QFormLayout::LabelRole, verticalSpacer_2);

        label_16 = new QLabel(centralWidget);
        label_16->setObjectName(QStringLiteral("label_16"));

        formLayout->setWidget(22, QFormLayout::LabelRole, label_16);

        doubleSpinBox_skalovanie = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_skalovanie->setObjectName(QStringLiteral("doubleSpinBox_skalovanie"));
        doubleSpinBox_skalovanie->setWrapping(false);
        doubleSpinBox_skalovanie->setFrame(true);
        doubleSpinBox_skalovanie->setAccelerated(false);
        doubleSpinBox_skalovanie->setCorrectionMode(QAbstractSpinBox::CorrectToPreviousValue);
        doubleSpinBox_skalovanie->setMinimum(-10);

        formLayout->setWidget(23, QFormLayout::LabelRole, doubleSpinBox_skalovanie);

        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        formLayout->setWidget(23, QFormLayout::FieldRole, pushButton_4);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(24, QFormLayout::LabelRole, verticalSpacer_3);

        label_18 = new QLabel(centralWidget);
        label_18->setObjectName(QStringLiteral("label_18"));

        formLayout->setWidget(25, QFormLayout::LabelRole, label_18);

        doubleSpinBox_skosenie = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_skosenie->setObjectName(QStringLiteral("doubleSpinBox_skosenie"));
        doubleSpinBox_skosenie->setMinimum(-5);

        formLayout->setWidget(26, QFormLayout::LabelRole, doubleSpinBox_skosenie);

        comboBox_skosenie = new QComboBox(centralWidget);
        comboBox_skosenie->setObjectName(QStringLiteral("comboBox_skosenie"));

        formLayout->setWidget(26, QFormLayout::FieldRole, comboBox_skosenie);

        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        formLayout->setWidget(27, QFormLayout::FieldRole, pushButton_5);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(29, QFormLayout::LabelRole, verticalSpacer_4);

        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout->setWidget(30, QFormLayout::LabelRole, label_13);

        comboBox_preklopenie = new QComboBox(centralWidget);
        comboBox_preklopenie->setObjectName(QStringLiteral("comboBox_preklopenie"));

        formLayout->setWidget(31, QFormLayout::LabelRole, comboBox_preklopenie);

        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        formLayout->setWidget(31, QFormLayout::FieldRole, pushButton_6);

        label_19 = new QLabel(centralWidget);
        label_19->setObjectName(QStringLiteral("label_19"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_19);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_8);

        pushButton_kruznica = new QPushButton(centralWidget);
        pushButton_kruznica->setObjectName(QStringLiteral("pushButton_kruznica"));

        formLayout->setWidget(5, QFormLayout::FieldRole, pushButton_kruznica);


        formLayout_2->setLayout(0, QFormLayout::LabelRole, formLayout);

        MyPainterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 657, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEffects = new QMenu(menuBar);
        menuEffects->setObjectName(QStringLiteral("menuEffects"));
        MyPainterClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEffects->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuEffects->addAction(actionClear);

        retranslateUi(MyPainterClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), MyPainterClass, SLOT(ActionOpen()));
        QObject::connect(actionClear, SIGNAL(triggered()), MyPainterClass, SLOT(EffectClear()));
        QObject::connect(actionNew, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNew()));
        QObject::connect(pushButton, SIGNAL(clicked()), MyPainterClass, SLOT(Kresli()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(ActionRotaciaVlavo()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), MyPainterClass, SLOT(ActionSkalovanie()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), MyPainterClass, SLOT(ActionRotaciaVpravo()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), MyPainterClass, SLOT(ActionSkosenie()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), MyPainterClass, SLOT(ActionPreklopenie()));
        QObject::connect(pushButton_kruznica, SIGNAL(clicked()), MyPainterClass, SLOT(ActionKruznica()));
        QObject::connect(pushButton_7, SIGNAL(clicked(bool)), MyPainterClass, SLOT(ActionVypln()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", 0));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", 0));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", 0));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", 0));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", 0));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", 0));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", 0));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", 0));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", 0));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", 0));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", 0));
        label_9->setText(QApplication::translate("MyPainterClass", "ALGORITMUS", 0));
        comboBox_algoritmus->clear();
        comboBox_algoritmus->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "DDA", 0)
         << QApplication::translate("MyPainterClass", "Bresenham", 0)
        );
        label_20->setText(QApplication::translate("MyPainterClass", "Pocet bodov kruznice", 0));
        label_5->setText(QApplication::translate("MyPainterClass", "Red", 0));
        label_6->setText(QApplication::translate("MyPainterClass", "Green", 0));
        label_7->setText(QApplication::translate("MyPainterClass", "Blue", 0));
        pushButton->setText(QApplication::translate("MyPainterClass", "Kresli utvar", 0));
        label_4->setText(QApplication::translate("MyPainterClass", "Farba vyplne", 0));
        label_10->setText(QApplication::translate("MyPainterClass", "Red", 0));
        label_11->setText(QApplication::translate("MyPainterClass", "Green", 0));
        label_12->setText(QApplication::translate("MyPainterClass", "Blue", 0));
        pushButton_7->setText(QApplication::translate("MyPainterClass", "Vypln", 0));
        label_15->setText(QApplication::translate("MyPainterClass", "hodnota", 0));
        label_2->setText(QApplication::translate("MyPainterClass", "Uhol otocenia", 0));
        label_3->setText(QApplication::translate("MyPainterClass", "               PI", 0));
        label_14->setText(QApplication::translate("MyPainterClass", "      Zadaj    *    alebo   /  ", 0));
        label->setText(QApplication::translate("MyPainterClass", "ROTACIA", 0));
        pushButton_2->setText(QApplication::translate("MyPainterClass", "       Rotacia vlavo        ", 0));
        pushButton_3->setText(QApplication::translate("MyPainterClass", "Rotacia vpravo", 0));
        label_16->setText(QApplication::translate("MyPainterClass", "SKALOVANIE", 0));
        pushButton_4->setText(QApplication::translate("MyPainterClass", "Skaluj", 0));
        label_18->setText(QApplication::translate("MyPainterClass", "SKOSENIE", 0));
        comboBox_skosenie->clear();
        comboBox_skosenie->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "V smere osi x", 0)
         << QApplication::translate("MyPainterClass", "V smere osi y", 0)
        );
        pushButton_5->setText(QApplication::translate("MyPainterClass", "Skosit", 0));
        label_13->setText(QApplication::translate("MyPainterClass", "PREKLOPENIE", 0));
        comboBox_preklopenie->clear();
        comboBox_preklopenie->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "Horizontalne", 0)
         << QApplication::translate("MyPainterClass", "Vertikalne", 0)
        );
        pushButton_6->setText(QApplication::translate("MyPainterClass", "Preklopit", 0));
        label_19->setText(QApplication::translate("MyPainterClass", "Polomer kruznice", 0));
        label_8->setText(QApplication::translate("MyPainterClass", "Farba hran", 0));
        pushButton_kruznica->setText(QApplication::translate("MyPainterClass", "Kresli kruznicu", 0));
        menuFile->setTitle(QApplication::translate("MyPainterClass", "File", 0));
        menuEffects->setTitle(QApplication::translate("MyPainterClass", "Effects", 0));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
